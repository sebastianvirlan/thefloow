class AddProgrammingLanguageToArticles < ActiveRecord::Migration[5.1]
  def change
    add_reference :articles, :programming_language, foreign_key: true, after: :user_id
  end
end
