Rails.application.routes.draw do
  resources :articles
  devise_for :users, path: ''
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  root to: 'articles#index'

  get '/my_articles', to: 'articles#my_articles'
  get '/articles_without_datatable', to: 'articles#articles_without_datatable'
  
end
