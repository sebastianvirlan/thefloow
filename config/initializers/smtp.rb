Rails.application.configure do
    config.action_mailer.smtp_settings = config_for(:smtp).symbolize_keys
end

Rails.application.configure do
    config.action_mailer.default_url_options = config_for(:mail_host).symbolize_keys
end