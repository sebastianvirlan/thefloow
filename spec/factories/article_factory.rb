FactoryGirl.define do
    factory :article do
        user { User.first || association(:user) }
        programming_language { ProgrammingLanguage.first || association(:programming_language) }
        title "Article Test"
        content "Content Test"
    end
end