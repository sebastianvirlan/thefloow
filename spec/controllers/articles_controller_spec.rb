require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  describe 'GET articles' do
    it 'should get all articles' do
      get :index
      expect(response).to render_template(:index)
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST create' do
    create_programming_language
    
    before(:each) do
      @new_article_attr = { :article => {
        title: 'Test Article',
        content: "My first Article content!",
        programming_language_id: programming_language.id 
        } 
      }
    end
  
    context 'With logged user and valid params' do
      sign_in_user_and_expect_that
      it 'should insert an article' do
        expect { 
          post :create, params: @new_article_attr
        }.to change(Article, :count).by(1) 
        expect(assigns(:article)).to be_a(Article)
        expect(response).to redirect_to(article_path(assigns[:article]))
        expect(response).to have_http_status(302)                
      end
    end

    context 'Without logged user and valid params' do
      it 'should not insert an article' do
        expect(subject.current_user).to eq(nil)
        expect { 
          post :create, params: @new_article_attr
        }.to change(Article, :count).by(0) 
        expect(response).to redirect_to(new_user_session_path())
        expect(response).to have_http_status(302)                
      end
    end

    context 'With logged user and invalid params,' do
      sign_in_user_and_expect_that
      after do
        expect { 
          post :create, params: @new_article_attr
        }.to change(Article, :count).by(0)
        expect(assigns(:article)).to be_a(Article)
        expect(response).to render_template(:new)
        expect(response).to have_http_status(200)
      end
      it 'without title should not insert an article' do
        @new_article_attr[:article][:title] = nil
      end
      it 'without content should not insert an article' do
        @new_article_attr[:article][:content] = nil
      end
      it 'without programming language should not insert an article' do
        @new_article_attr[:article][:programming_language_id] = nil
      end
    end
  end


  describe 'PUT update' do

    before(:each) do
      @article = create(:article)
      @programming_language = ProgrammingLanguage.first
      sign_in User.first
      @article_attrs = 
        {
          id: @article.id,
          article: {
            title: 'Test Article',
            content: "My first Article content!",
            programming_language_id: @programming_language.id,
            user_id: subject.current_user.id
          }
        }
    end

    context 'With logged user and valid params' do
      it 'should update an article' do
        put :update, params: @article_attrs
        @article.reload

        expect(assigns(:article)).to be_a(Article)
        expect(response).to redirect_to(assigns[:article])
        expect(@article.title).to eq(@article_attrs[:article][:title])
        expect(@article.content).to eq(@article_attrs[:article][:content])
        expect(response).to have_http_status(302)                
      end
    end

    context 'With logged user and invalid params,' do

      after do
        put :update, params: @article_attrs
        @article.reload

        expect(@article.title).to_not eq(@article_attrs[:article][:title])
        expect(@article.content).to_not eq(@article_attrs[:article][:content])
        expect(response).to render_template(:edit)                
        expect(response).to have_http_status(200)
      end

      it 'without title should not update the article' do
        @article_attrs[:article][:title] = nil
      end
      it 'without content should not update the article' do
        @article_attrs[:article][:content] = nil
      end
      it 'without programming language should not update the article' do
        @article_attrs[:article][:programming_language_id] = nil
      end
      it 'but not the owner should not update the article' do
        @article_attrs[:article][:programming_language_id] = 0
      end
    end
  end

  describe 'DELETE destroy' do

    before(:each) do
      @article = create(:article)
      sign_in User.first
    end

    context 'With logged user and valid params' do
      it 'should destroy an article' do
        delete :destroy, params: { id: @article }
        expect(response).to redirect_to(articles_url)
        expect(flash[:notice]).to match(/Article was successfully destroyed.*/)         
      end
    end

    context 'with logged user and invalid params' do
      it 'but not the owner should not destroy an article' do
        user = create(:user)
        sign_in user

        delete :destroy, params: { id: @article }
        expect(response).to redirect_to(articles_url)
        expect(flash[:alert]).to match(/Action unsuccessful, you are not the owner.*/)
      end
    end
  end
end
