module ControllerMacros
  def each_login_user
    before(:each) do
      user = FactoryGirl.create(:user)
      sign_in user
    end
  end

  def sign_in_user_and_expect_that
      before do
          sign_in create(:user)
          expect(subject.current_user).to_not eq(nil)
      end
  end

  def expect_user_login
    expect(subject.current_user).to_not eq(nil)
  end
  
  def expectations_for_invalid_article
    expect(assigns(:article)).to be_a(Article)
    expect(response).to render_template(:new)
    expect(response).to have_http_status(200)
  end
  
  def create_programming_language
    let(:programming_language) { create(:programming_language) }
  end
end