class Article < ApplicationRecord
  validates :title, presence: true
  validates :content, presence: true
  validates :programming_language_id, presence: true

  belongs_to :user
  belongs_to :programming_language

  scope :search_by_title_and_programming_language, ->(query_string) { 
    joins(:programming_language).where("articles.title LIKE :search_query OR programming_languages.name LIKE :search_query", search_query: query_string)
  }
end
