class ArticleDatatable < AjaxDatatablesRails::Base

  def_delegators :@view, :link_to, :edit_article_path, :user_signed_in?, :render

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "Article.id", cond: :eq },
      title: { source: "Article.title", searchable: true, cond: :like },
      programming_language: { source: "ProgrammingLanguage.name", searchable: true, cond: :like },
      creator: { source: "User.email", searchable: true, cond: :like }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        id: record.id,
        title: record.title,
        creator: record.user.email,
        programming_language: record.programming_language.name,
        actions: render('articles/actions.html.erb', {article: record})
      }
    end
  end

  private

  def get_raw_records
    # insert query here
    articles = Article.joins(:user, :programming_language).order(id: :desc)
    articles = articles.where(user_id: options[:user].id) unless options[:user].nil?
    
    articles
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
