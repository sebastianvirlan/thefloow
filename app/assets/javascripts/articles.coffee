# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
articles_table = null
$(document).on "turbolinks:load", ->
  articles_table = $('#articles-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#articles-table').data('source')
    pagingType: 'full_numbers'
    columns: [
      {data: 'id'}
      {data: 'title'}
      {data: 'programming_language'}
      {data: 'creator'}
      {data: 'actions'}
    ]
  ready()
      # pagingType is optional, if you want full pagination controls.
      # Check dataTables documentation to learn more about
      # available options.

document.addEventListener 'turbolinks:before-cache', ->
  if articles_table != null
   articles_table.fnDestroy();
   articles_table = null;
  

ready = ->
  hljs.initHighlighting.called = false;
  hljs.initHighlighting();
