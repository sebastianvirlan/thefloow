class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /articles
  # GET /articles.json
  def index
    @title = "All Articles"
    respond_to do |format|
      format.json { render json: ArticleDatatable.new(view_context) }
      format.html
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
      unless is_owner? @article
        redirect_to @article, alert: 'Action unsuccessful, you are not the owner.'
      end
  end

  # POST /articles
  # POST /articles.json
  def create
    data = article_params.merge({user_id: current_user.id})
    @article = Article.new(data)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|

      unless is_owner? @article
        format.html { redirect_to @article, alert: 'Action unsuccessful, you are not the owner.'}
        format.json { render :show, status: :error, message: :not_owner }
      else
        if @article.update(article_params)
          format.html { redirect_to @article, notice: 'Article was successfully updated.' }
          format.json { render :show, status: :ok, location: @article }
        else
          format.html { render :edit }
          format.json { render json: @article.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    respond_to do |format|

      unless is_owner? @article
        format.html { redirect_to articles_url, alert: 'Action unsuccessful, you are not the owner.'}
        format.json { render :show, status: :error, message: :not_owner }
      else
        @article.destroy
        format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  def my_articles
    @title = "My Articles"    
    respond_to do |format|
      Article.find_by(user_id: current_user.id)
      format.html {render 'articles/index' }
      format.json { render json: ArticleDatatable.new(view_context, { user: current_user }) }
    end 
  end

  def articles_without_datatable
    @articles = Article.order(id: :desc)

    unless params[:search].blank?
      @articles = @articles.search_by_title_and_programming_language("%#{params[:search]}%")
    end

    @articles = @articles.page(params[:page]).per(1)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :content, :programming_language_id)
    end

    def require_permission
      if current_user != Article.find(params[:id]).user
        redirect_to article_path
      end
    end

    def is_owner? article
      current_user == article.user ? true : false
    end
end
