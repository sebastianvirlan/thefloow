#!/bin/bash

if ! gem spec "rails" > /dev/null 2>&1; then
    gem install rails
fi

if [ ! -f /usr/src/app/Gemfile ]; then
    rails new .
fi

bundle install
shutup
rails s -b 0.0.0.0