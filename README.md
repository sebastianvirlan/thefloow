## VIA DOCKER ##

```
#!bash



git clone https://sebastianvirlan@bitbucket.org/sebastianvirlan/thefloow.git
# Copy the configuration examples from `config/examples` into `config`, mail and database configurations (for database you should let the credentials from the example file because docker will create a mysql container with those credentials)
cd thefloow/Docker
docker-compose up --buid -d
#will do automatically bundle install

```

Project should start at localhost:3000

Get all running containers and copy the rails application container

docker ps

The image should be something like this:


```
#!code

RAILS_IMAGE_ID        docker_thefloow-app     "/home/cmd.sh"           26 hours ago        Up 16 minutes       0.0.0.0:3000->3000/tcp   docker_thefloow-app_1

```


Access the application container


```
#!bash

docker exec -it RAILS_IMAGE_ID

```


You should be already in `/usr/src/app`, run the migrations and the seeds:


```
#!bash

rails db:migrate
rails db:seed
```


Access http://localhost:3000/ and you should see the application.

## WITHOUT DOCKER ##


```
#!bash

git clone https://sebastianvirlan@bitbucket.org/sebastianvirlan/thefloow.git

```

Copy the configuration examples from `config/examples` into `config` and populate with your settings.


```
#!bash

bundle
rails db:migrate
rails db:seed
rails s
```


Access http://localhost:3000/ and you should see the application.


Run tests with:


```
#!bash

rspec

```

## External asset resources:  ##

- Responsive table: https://codepen.io/zavoloklom/pen/IGkDz
- Animated search input: https://www.w3schools.com/howto/howto_css_animated_search.asp
- Login / Register form: https://codepen.io/colorlib/pen/rxddKy

![Screen Shot 2017-06-21 at 17.23.15.png](https://bitbucket.org/repo/9p8456G/images/4285708288-Screen%20Shot%202017-06-21%20at%2017.23.15.png)
![Screen Shot 2017-06-21 at 02.31.39.png](https://bitbucket.org/repo/9p8456G/images/3890170431-Screen%20Shot%202017-06-21%20at%2002.31.39.png)
![Screen Shot 2017-06-21 at 02.14.04.png](https://bitbucket.org/repo/9p8456G/images/1826343858-Screen%20Shot%202017-06-21%20at%2002.14.04.png)